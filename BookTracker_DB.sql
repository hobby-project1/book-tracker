-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 03, 2019 at 06:03 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BookTracker_DB`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `book_id` int(11) NOT NULL,
  `book_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fk_book_category_id` int(11) DEFAULT NULL,
  `fk_book_author_id` int(11) DEFAULT NULL,
  `book_description` longtext COLLATE utf8_unicode_ci,
  `fk_bookinfo_uploader_user_id` int(11) DEFAULT NULL,
  `book_publish_year` int(11) DEFAULT NULL,
  `book_page` int(11) DEFAULT NULL,
  `book_rating` float DEFAULT NULL,
  `book_image` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `book_status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `book_created_at` timestamp NULL DEFAULT NULL,
  `book_updated_at` timestamp NULL DEFAULT NULL,
  `book_deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`book_id`, `book_name`, `fk_book_category_id`, `fk_book_author_id`, `book_description`, `fk_bookinfo_uploader_user_id`, `book_publish_year`, `book_page`, `book_rating`, `book_image`, `book_status`, `book_created_at`, `book_updated_at`, `book_deleted_at`) VALUES
(4, 'নির্বাসন update', 13, 1, NULL, 2, 2019, 201, NULL, '4.jpg', 'Active', '2019-05-20 12:40:28', '2019-06-26 06:56:22', NULL),
(5, 'প্যারাডক্সিক্যাল সাজিদ', 2, 1, NULL, 2, 2017, NULL, NULL, '5.jpg', 'Active', '2019-05-20 13:25:35', '2019-05-20 13:25:35', NULL),
(6, 'ওয়াসওয়াসা : শয়তানের কুমন্ত্রণা', 2, 2, NULL, 2, 1996, NULL, NULL, '6.jpg', 'Active', '2019-05-25 13:21:13', '2019-05-25 13:21:13', NULL),
(7, 'ভাল্লাগে না', 3, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 2, 2003, NULL, NULL, '7.jpg', 'Active', '2019-05-25 13:28:33', '2019-06-15 12:14:50', NULL),
(8, 'higenburger golpo', 1, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries', 2, 2012, NULL, NULL, '8.jpg', 'Active', '2019-05-25 13:29:11', '2019-06-15 10:40:31', NULL),
(9, 'রিচ ড্যাড পুওর ড্যাড', 1, 1, 'প্রয়োজনীয়তা স্কুল কি বাস্তব দুনিয়ার জন্য ছেলেমেয়েদের তৈরি করে? আমার বাবা-মা বলতেন, “পড়াশুনা কর, ভালো গ্রেড পাও, দেখবে তুমি অনেক বড় চাকরি পাবে।” তাদের। জীবনের লক্ষ্য ছিল আমাকে ও আমার বড় বোনকে কলেজে পাঠাননা, যাতে জীবনে সফল হওয়ার সবচেয়ে বেশি সুযোগ পাই। ১৯৭৬ এ আমি যখন ফ্লোরিডা স্টেট ইউনিভার্সিটি থেকে একাউটিং এ সর্বোচ্চ নম্বর নিয়ে অনার্স গ্রাজুয়েট হলাম, আমার বাবা-মা তাদের লক্ষ্যে পৌছে গেলেন। তাঁদের জীবনের চূড়ান্ত আনন্দের ছিল এই অর্জন। বিগ এইট’ নামক এক একাউন্টিং', 2, 2019, 200, NULL, '9.jpg', 'Active', '2019-06-25 23:16:05', '2019-06-25 23:16:05', NULL),
(10, 'ভালো ছেলে আর নই', 2, 2, 'ভালো ছেলে আর নই : নো মোর মিস্টার নাইস গাই', 2, 1991, 201, NULL, '10.jpg', 'Active', '2019-06-25 23:17:00', '2019-06-25 23:17:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_author`
--

CREATE TABLE `book_author` (
  `author_id` int(11) NOT NULL,
  `author_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `author_photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_author_info_uploader_user_id` int(11) DEFAULT NULL,
  `author_created_at` timestamp NULL DEFAULT NULL,
  `author_updated_at` timestamp NULL DEFAULT NULL,
  `author_deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_author`
--

INSERT INTO `book_author` (`author_id`, `author_name`, `author_photo`, `fk_author_info_uploader_user_id`, `author_created_at`, `author_updated_at`, `author_deleted_at`) VALUES
(1, 'Arabi Kabir', '1.jpg', 1, '2019-05-19 11:41:33', '2019-05-19 11:41:33', NULL),
(2, 'Latif kabir', '2.jpg', 1, '2019-05-19 11:42:16', '2019-05-19 11:42:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_category`
--

CREATE TABLE `book_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fk_creator_user_id` int(11) DEFAULT NULL,
  `category_created_at` timestamp NULL DEFAULT NULL,
  `category_updated_at` timestamp NULL DEFAULT NULL,
  `category_deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_category`
--

INSERT INTO `book_category` (`category_id`, `category_name`, `fk_creator_user_id`, `category_created_at`, `category_updated_at`, `category_deleted_at`) VALUES
(1, 'Adventure', 2, '2019-05-18 21:21:12', '2019-05-18 21:00:17', NULL),
(2, 'Horror', 2, '2019-05-13 21:08:00', '2019-05-18 20:15:00', NULL),
(3, 'Comedy', 1, '2019-05-19 09:23:25', '2019-05-19 09:23:25', NULL),
(4, 'cat 1', 1, '2019-05-19 09:24:03', '2019-05-19 09:24:03', '2019-05-19 12:16:33'),
(5, 'cat 2', 1, '2019-05-19 09:26:39', '2019-05-19 09:26:39', '2019-05-19 12:16:29'),
(6, 'cat 3', 1, '2019-05-19 09:27:10', '2019-05-19 09:27:10', '2019-05-19 12:16:26'),
(7, 'cat 5', 1, '2019-05-19 09:28:26', '2019-05-19 09:28:26', '2019-05-19 12:16:22'),
(8, 'cat 7', 1, '2019-05-19 09:28:40', '2019-05-19 09:28:40', '2019-05-19 10:54:17'),
(9, 'cat 5', 1, '2019-05-19 09:29:40', '2019-05-19 09:29:40', '2019-05-19 10:53:45'),
(10, 'cat 55', 1, '2019-05-19 09:30:16', '2019-05-19 09:30:16', '2019-05-19 10:53:42'),
(11, 'cat 77 update', 1, '2019-05-19 09:30:41', '2019-05-19 09:30:41', '2019-05-19 10:53:35'),
(12, 'new cat', 1, '2019-05-19 10:54:36', '2019-05-19 10:54:36', '2019-05-19 12:16:19'),
(13, 'Action', 1, '2019-05-19 12:16:44', '2019-05-19 12:16:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_review`
--

CREATE TABLE `book_review` (
  `review_id` int(11) NOT NULL,
  `fk_book_id` int(11) NOT NULL,
  `fk_reviewer_user_id` int(11) NOT NULL,
  `review` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_review`
--

INSERT INTO `book_review` (`review_id`, `fk_book_id`, `fk_reviewer_user_id`, `review`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 8, 2, 'review 1', '2019-06-15 11:48:30', '2019-06-15 11:48:30', NULL),
(6, 8, 2, 'review 2', '2019-06-15 11:48:36', '2019-06-15 11:48:36', NULL),
(7, 8, 2, 'review 3', '2019-06-15 11:55:18', '2019-06-15 11:55:18', NULL),
(8, 8, 2, 'review 4', '2019-06-15 11:58:29', '2019-06-15 11:58:29', NULL),
(9, 8, 2, 'review 5', '2019-06-15 11:58:33', '2019-06-15 11:58:33', NULL),
(10, 8, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries', '2019-06-15 11:59:05', '2019-06-15 11:59:05', NULL),
(11, 7, 2, 'New review', '2019-06-15 12:10:46', '2019-06-15 12:10:46', NULL),
(12, 4, 2, 'valo book', '2019-06-15 21:47:27', '2019-06-15 21:47:27', NULL),
(13, 4, 2, 'onek valo', '2019-06-15 21:47:41', '2019-06-15 21:47:41', NULL),
(14, 4, 2, 'test 1', '2019-06-23 03:19:14', '2019-06-23 03:19:14', NULL),
(15, 5, 2, 'he', '2019-06-25 11:41:46', '2019-06-25 11:41:46', NULL),
(16, 6, 2, 'rgh', '2019-06-25 12:15:50', '2019-06-25 12:15:50', NULL),
(17, 6, 2, 'DJ', '2019-06-25 12:16:50', '2019-06-25 12:16:50', NULL),
(18, 10, 2, 'hi', '2019-06-26 07:01:20', '2019-06-26 07:01:20', NULL),
(19, 5, 2, 'ke', '2019-06-26 22:59:07', '2019-06-26 22:59:07', NULL),
(20, 4, 2, 'yo', '2019-06-27 00:52:34', '2019-06-27 00:52:34', NULL),
(21, 6, 6, 'DJ Khaled', '2019-07-02 09:08:55', '2019-07-02 09:08:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `collected_books`
--

CREATE TABLE `collected_books` (
  `collected_books_id` int(11) NOT NULL,
  `fk_book_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `availability` int(11) DEFAULT NULL COMMENT '0=not available, 1=available'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `collected_books`
--

INSERT INTO `collected_books` (`collected_books_id`, `fk_book_id`, `fk_user_id`, `availability`) VALUES
(5, 7, 2, 0),
(6, 5, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `complete_books`
--

CREATE TABLE `complete_books` (
  `complete_books_id` int(11) NOT NULL,
  `fk_book_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `complete_date` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `complete_books`
--

INSERT INTO `complete_books` (`complete_books_id`, `fk_book_id`, `fk_user_id`, `complete_date`, `deleted_at`) VALUES
(28, 6, 2, NULL, NULL),
(32, 7, 2, NULL, NULL),
(33, 4, 2, NULL, NULL),
(35, 9, 2, NULL, NULL),
(36, 10, 2, NULL, NULL),
(37, 5, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `friend_relation`
--

CREATE TABLE `friend_relation` (
  `friend_relation_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_friend_user_id` int(11) NOT NULL,
  `request_status` int(11) NOT NULL COMMENT '0=Pending, 1=Approved, 2=Reject',
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `friend_relation`
--

INSERT INTO `friend_relation` (`friend_relation_id`, `fk_user_id`, `fk_friend_user_id`, `request_status`, `created_at`, `deleted_at`) VALUES
(1, 13, 2, 1, '2019-12-03 16:56:09', NULL),
(2, 2, 13, 1, '2019-12-03 16:57:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lend_book`
--

CREATE TABLE `lend_book` (
  `lend_book_id` int(11) NOT NULL,
  `fk_book_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `lent_to` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lend_date` date DEFAULT NULL,
  `expected_return_date` date DEFAULT NULL,
  `returned_date` date DEFAULT NULL,
  `lend_status` int(11) NOT NULL COMMENT '0=OK, 1=Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lend_book`
--

INSERT INTO `lend_book` (`lend_book_id`, `fk_book_id`, `fk_user_id`, `lent_to`, `lend_date`, `expected_return_date`, `returned_date`, `lend_status`, `created_at`, `deleted_at`) VALUES
(5, 7, 2, 'Rakib', '2019-06-19', '2019-06-29', NULL, 1, '2019-06-27 13:15:37', NULL),
(6, 4, 2, 'Moin', '2019-06-19', '2019-06-30', '2019-06-29', 0, '2019-06-27 13:26:13', NULL),
(7, 5, 2, 'Moin', '2019-06-19', '2019-06-29', NULL, 1, '2019-06-27 13:28:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reading_books`
--

CREATE TABLE `reading_books` (
  `reading_books_id` int(11) NOT NULL,
  `fk_book_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reading_books`
--

INSERT INTO `reading_books` (`reading_books_id`, `fk_book_id`, `fk_user_id`) VALUES
(6, 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userType` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.png',
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userType`, `name`, `profile_image`, `description`, `email`, `designation`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Arabi', 'avatar.png', NULL, 'admin@gmail.com', 'Software Developer', NULL, '$2y$10$KvrQXbU2HEOArN.5uRXKb.PMGd95ew.IgBZ5h3bpx7flofO.GZn8i', NULL, '2019-05-15 13:26:57', '2019-05-15 13:26:57'),
(2, 'user', 'Kabir Khan', '2.jpg', 'Lorem ipsum dolor sit amet, nibh facer necessitatibus an pri, delectus scaevola honestatis eos ea. Usu affert munere inciderint cu. Vel ne wisi consetetur. Usu laudem oblique cu. Has ne dolor interpretaris, maluisset consetetur consectetuer at ius, pri ludus bonorum civibus ex. Vim an meis ullum deserunt, usu clita nusquam ex.', 'kabir@gmail.com', 'Software Developer', NULL, '$2y$10$RoaqADPEvBfXPbCUUKvDvO6BFYlRgbQ3MCcBvEMfmFIsE9UV8doAi', NULL, '2019-05-18 23:57:38', '2019-06-29 03:15:28'),
(3, 'user', 'Sadik Kabir', '3.jpg', NULL, 'sadik@gmail.com', NULL, NULL, '$2y$10$otvQ/nN00Ivu1cnsq8Jh4O5ZwcYAdXK9CuQN.Md/RJJqReI30EMAC', NULL, '2019-06-29 12:03:10', '2019-06-29 12:30:44'),
(4, 'user', 'Moin', '4.jpg', NULL, 'moin@gmail.com', 'HR manager', NULL, '$2y$10$CHFWJv3nMsOmBKQ3pzUn3eGEThLxnTbhDd.jazlCsFGPSPQEwe81O', NULL, '2019-06-29 12:03:30', '2019-06-29 12:31:19'),
(5, 'user', 'Antu', 'avatar.png', NULL, 'antu@gmail.com', 'Gamer', NULL, '$2y$10$JWN9nV6ZzjiQpx8MeIqJ7eofRhiuZ4sItjW6kxa/9ywKvZ5KY3HUe', NULL, '2019-06-29 12:03:56', '2019-06-29 12:03:56'),
(6, 'user', 'Ruhan', '6.jpg', 'Opps !!', 'ruhan@gmail.com', 'Designer', NULL, '$2y$10$Z/MHLjG43nQvEdRPCOWVVemjIc6xXiduoI/.AEi/wLSIAxtqVfLpS', NULL, '2019-06-30 11:18:11', '2019-06-30 11:19:36'),
(7, 'user', 'New Antu', '7.jpg', NULL, 'newantu@mail.com', NULL, NULL, '$2y$10$gz4hhO9169hJLJJsLqpcy.Z35DhYDuKAjNltR1Nr4TrV8eFXuzx2G', NULL, '2019-07-01 12:22:13', '2019-07-01 12:22:28'),
(8, 'user', 'ustho', '8.jpg', NULL, 'utsho@mail.com', NULL, NULL, '$2y$10$62Q9FZ6krZn/pw/dTmWRQelIt1ERHVqV/pH6txit.S/TJWJexE8Qu', NULL, '2019-07-01 12:23:00', '2019-07-01 12:23:16'),
(9, 'user', 'Rahedi', '9.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt lectus sed viverra interdum. Aliquam erat volutpat. Duis in quam lacinia, sollicitudin sem at, vehicula augue. Sed rhoncus ante at mauris ultricies vestibulum ut id risus. Sed quam dolor, pellentesque sodales nibh quis, egestas consectetur sapien. Nulla consequat auctor arcu at maximus. Maecenas vulputate egestas risus. Fusce felis leo, tempor scelerisque mauris ac, varius varius ipsum. Nunc ac quam tempor ligula convallis ultricies. In et ultrices eros, eget lobortis odio. Aenean porta, diam ac cursus iaculis, ligula ante efficitur nibh, nec porta augue ante in neque. Praesent aliquam eros et odio venenatis auctor. In lacinia lectus ne', 'rahedi@gmail.com', 'First Boy', NULL, '$2y$10$7zQOFmv7VRgLD.wc2PsttOZG22dQvSno9Xx.aoKozBR0hf.SQdbHW', NULL, '2019-07-01 13:01:19', '2019-07-01 13:03:43'),
(10, 'user', 'Habib', '10.jpg', NULL, 'habib@gmail.com', NULL, NULL, '$2y$10$PgiawzCv.1PnxmRLNUlnyOiiqGQSsUsCILfBq6XUBSPTdbc4GRjcq', NULL, '2019-07-01 13:08:24', '2019-07-01 13:08:35'),
(11, 'user', 'Tanbir', '11.jpg', NULL, 'tanbir@gmail.com', NULL, NULL, '$2y$10$H1sOjlYfVMvbLMhQ.zq9kO3qhMF.4PkJFRctAJo7KDtzcgSOsiSkq', NULL, '2019-07-01 13:09:01', '2019-07-01 13:09:37'),
(12, 'user', 'Ela', '12.jpg', NULL, 'ela@gmail.com', NULL, NULL, '$2y$10$BbOszxY0JWmtVlkPi6E0FO2ZixopCI2d/ud4ZaEXAvdBbVZXhEJra', NULL, '2019-07-01 13:10:00', '2019-07-01 13:10:10'),
(13, 'user', 'Rumi', '13.jpg', NULL, 'rumi@gmail.com', NULL, NULL, '$2y$10$n6uj/iZv9tntDSt9SqMjYud.y.kZayRfmrT/gEPxv2zGUi0LHkf2.', NULL, '2019-07-01 13:10:28', '2019-07-01 13:10:39'),
(14, 'user', 'Pollob', '14.jpg', NULL, 'pollob@gmail.com', NULL, NULL, '$2y$10$dYNZSMCmyFHv3fl.9YPckuWfF6mO/C4VDT/nnBfQXgVw3NMDnsLmS', NULL, '2019-07-01 13:11:02', '2019-07-01 13:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist_books`
--

CREATE TABLE `wishlist_books` (
  `wishlist_books_id` int(11) NOT NULL,
  `fk_book_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `wishlist_added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wishlist_books`
--

INSERT INTO `wishlist_books` (`wishlist_books_id`, `fk_book_id`, `fk_user_id`, `wishlist_added_date`) VALUES
(4, 6, 2, '2019-06-25'),
(6, 8, 2, '2019-06-27'),
(7, 10, 2, '2019-06-27'),
(8, 4, 2, '2019-07-02'),
(9, 6, 6, '2019-07-02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `book_author`
--
ALTER TABLE `book_author`
  ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `book_category`
--
ALTER TABLE `book_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `book_review`
--
ALTER TABLE `book_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `collected_books`
--
ALTER TABLE `collected_books`
  ADD PRIMARY KEY (`collected_books_id`);

--
-- Indexes for table `complete_books`
--
ALTER TABLE `complete_books`
  ADD PRIMARY KEY (`complete_books_id`);

--
-- Indexes for table `friend_relation`
--
ALTER TABLE `friend_relation`
  ADD PRIMARY KEY (`friend_relation_id`);

--
-- Indexes for table `lend_book`
--
ALTER TABLE `lend_book`
  ADD PRIMARY KEY (`lend_book_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `reading_books`
--
ALTER TABLE `reading_books`
  ADD PRIMARY KEY (`reading_books_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wishlist_books`
--
ALTER TABLE `wishlist_books`
  ADD PRIMARY KEY (`wishlist_books_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `book_author`
--
ALTER TABLE `book_author`
  MODIFY `author_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `book_category`
--
ALTER TABLE `book_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `book_review`
--
ALTER TABLE `book_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `collected_books`
--
ALTER TABLE `collected_books`
  MODIFY `collected_books_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `complete_books`
--
ALTER TABLE `complete_books`
  MODIFY `complete_books_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `friend_relation`
--
ALTER TABLE `friend_relation`
  MODIFY `friend_relation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lend_book`
--
ALTER TABLE `lend_book`
  MODIFY `lend_book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `reading_books`
--
ALTER TABLE `reading_books`
  MODIFY `reading_books_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wishlist_books`
--
ALTER TABLE `wishlist_books`
  MODIFY `wishlist_books_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
